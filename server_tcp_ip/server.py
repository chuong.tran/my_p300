# Server TCP/IP python code
# Signed-off by Chuong Tran <anhchuong89@gmail.com"

import time
# GPIO RPi 2 BCM SoC
import RPi.GPIO as GPIO
# TCP IP
import socket

GPIO_LEFT = 6
GPIO_RIGHT = 13
GPIO_FORWARD = 19
GPIO_BACKWARD = 26

global prev_data

def GPIO_setmode():
	# Set mode for Broadcom soc
	GPIO.setmode(GPIO.BCM)
	# Set GPIO mode
	GPIO.setup(GPIO_LEFT, GPIO.OUT)
	GPIO.setup(GPIO_RIGHT, GPIO.OUT)	
	GPIO.setup(GPIO_FORWARD, GPIO.OUT)	
	GPIO.setup(GPIO_BACKWARD, GPIO.OUT)

def GPIO_default():
	# State 1 = True (HIGH), 0 =  False (LOW)
	GPIO.output(GPIO_LEFT, 1)
	GPIO.output(GPIO_RIGHT, 1)
	GPIO.output(GPIO_FORWARD, 1)
	GPIO.output(GPIO_BACKWARD, 1)

def left(angle=0):
	round_trip_time = 2.8 # in seconds
	sleep_time = round_trip_time * angle / 360.0
	GPIO_default()
	# Set state
	GPIO.output(GPIO_LEFT, 0)
	time.sleep(sleep_time)
	# Clear state
	GPIO.output(GPIO_LEFT,1)

def right(angle=0):
	round_trip_time = 2.8 # in seconds
	sleep_time = round_trip_time * angle / 360.0
	GPIO_default()
	# Set state
	GPIO.output(GPIO_RIGHT, 0)
	time.sleep(sleep_time)
	# Clear state
	GPIO.output(GPIO_RIGHT, 1)

def forward():	
	GPIO.output(GPIO_FORWARD, 0)

def backward():
	GPIO.output(GPIO_BACKWARD, 0)

def process_data(data, previous_data):
	# data is received by socket
	# U = Forward, D = Backward
	# L = LEFT, R = RIGHT
	# UL = LEFT 45, DL = LEFT 135
	# UR = RIGHT 45, DR = RIGHT 135 
	# S = Stop 
	if data == 'S':
		GPIO_default()
	elif data == 'L':
		left(90.0)
	elif data == 'R':
		right(90.0)
	elif data == 'U':
		forward()
	elif data == 'D':
		backward()
	elif data == 'UL':
		left(45.0)
	elif data == 'DL':
		left(135.0)
	elif data == 'UR':
		right(45.0)
	elif data == 'DR':
		right(135.0)
	else:
		GPIO_default()

def main():
	TCP_IP='193.168.1.10'
	TCP_PORT=5005
	BUFFER_SIZE=20	# Normally 1024, but I want fast response

	prev_data = '0'

	# Set GPIO mode in default state
	GPIO_setmode()
	GPIO_default()

	# Socket binding
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((TCP_IP, TCP_PORT))
	s.listen(1)

	conn, addr = s.accept()
	print 'Connection address: ', addr
	while True:
        	data = conn.recv(BUFFER_SIZE)
        	if not data:
                	break
        	print "received data: " , data
		process_data(data, prev_data)
		prev_data = data
        	#conn.send(data)         # echo
		time.sleep(5)
		GPIO_default()

	conn.close()

if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		print "\nKeyboardInterrupt detected"	
	finally:
		# Clear state and clean up
		GPIO_default()
		GPIO.cleanup()
