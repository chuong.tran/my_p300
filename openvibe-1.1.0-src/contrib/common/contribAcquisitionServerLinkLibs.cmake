
INCLUDE("FindThirdPartyBrainmasterCodeMakerAPI")
INCLUDE("FindThirdPartyEemagineEEGO")
INCLUDE("FindThirdPartyGMobiLabPlusAPI")
INCLUDE("FindThirdPartyGUSBampCAPI")
INCLUDE("FindThirdPartyMitsar")
INCLUDE("FindThirdPartyEmokit")
ADD_DEFINITIONS(-DTARGET_HAS_ThirdPartyEmokit)
target_link_libraries (${PROJECT_NAME} /usr/local/lib/libemokit.a)
