# Signed-off by Chuong Tran <anhchuong@gmail.com> 
# Python code for Python Box to send target stimulation via TCP/IP

import numpy
import socket

class MyOVBox(OVBox):
	def __init__(self):
		OVBox.__init__(self)
		self.signalHeader = None
		self.nb_electrodes = 1
		self.ip = None
		self.port = None
		self.size = None
		self.socket = None
	
		self.row = None
		self.column = None	
		self.pending_stim = []
		self.pending_chunk = []

	def initialize(self):
		self.row = 0
		self.column = 0
		self.ip = str(self.setting['IP'])
		self.port = int(self.setting['PORT'])
		self.size = int(self.setting['BUFFER_SIZE'])
		print "Socket settings:", self.ip, self.port, self.size
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((self.ip, self.port))

	def uninitialize(self):
		self.socket.close()

	def process(self):
		sep = '\n'
		stimulation = [
					  [ 'UL', 'U', 'UR'	],
					  [ 'L' , 'S', 'R' 	],
					  [ 'DL', 'D', 'DR'	]	
					  ]
		# Get Row stimulation
		for chunkIndex in range( len(self.input[0]) ):
			if(type(self.input[0][chunkIndex]) == OVStimulationHeader):
				self.stimulation_header = self.input[0].pop()
				print 'Received chunk of type ', type(self.stimulation_header), " looking for StimulationSet"
			elif(type(self.input[0][chunkIndex]) == OVStimulationSet):
				chunk = self.input[0].pop()
				for stimIdx in range(len(chunk)):
					stim=chunk.pop();
					print 'At time ', stim.date, ' received row stim ', stim.identifier - 33024
					self.row = stim.identifier - 33024

		# Get Column stimulation		
		for chunkIndex in range( len(self.input[1]) ):
			if(type(self.input[1][chunkIndex]) == OVStimulationHeader):
				self.stimulation_header = self.input[1].pop()
				print 'Received chunk of type ', type(self.stimulation_header), " looking for StimulationSet"
			elif(type(self.input[1][chunkIndex]) == OVStimulationSet):
				chunk = self.input[1].pop()
				for stimIdx in range(len(chunk)):
					stim=chunk.pop();
					print 'At time ', stim.date, ' received column stim ', stim.identifier - 33030
					self.column = stim.identifier - 33030

		if self.row != 0 and self.column != 0:
			val = stimulation[self.row-1][self.column-1]
			print 'Letter ', val
			# Send stimulation via socket
			self.socket.send(val)
			# Clear row/column for next iteration
			self.row = 0
			self.column = 0

		return

box = MyOVBox()

"""
		stimulation = [ 
				[ 'a', 'b', 'c', 'd', 'e', 'f' ],
				[ 'g', 'h', 'i', 'j', 'k', 'l' ],
				[ 'm', 'n', 'o', 'p', 'q', 'r' ],
				[ 's', 't', 'u', 'v', 'w', 'x' ],
				[ 'y', 'z', '1', '2', '3', '4' ],
				[ '5', '6', '7', '8', '9', '0' ]
				]
"""
