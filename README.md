This project is for p300 response detection, using Emotiv EPOC device.

Signed-off by Chuong Tran <anhchuong89@gmail.com>

Also see at: https://gitlab.com/chuong.tran/my_p300/blob/master/README.md

/////////////////////////////

First, please use Ubuntu distribution as your Linux system. These instructions below are 
verified with ubuntu 15.04.

Refer to http://releases.ubuntu.com/15.04/ to download pre-install ubuntu
images. 

If your computer has 64 bit CPU (x86_64), please use:
http://releases.ubuntu.com/15.04/ubuntu-15.04-desktop-amd64.iso

If your computer has 32 bit CPU (i386), please use:
http://releases.ubuntu.com/15.04/ubuntu-15.04-desktop-i386.iso

NOTES: Recommend to have a parallel operation systems (Ubuntu + Windows), using
virtual machine with ubuntu might make your computer running very slow. Use
virtualbox to install virtual machine ubuntu in case you can not change anything
on your hard drive. 

Googling if you do not know how to install ubuntu on your computer.

If you have any questions, do not be hesitate to send email to:
anhchuong89@gmail.com. I will be willing to help you to solve any problems.

//////////////////////////////

1) Install some neccessary packages for Emotiv EPOC acquisition purpose
Emotiv EPOC need some libraries to get data buffer from USB receiver and decode
data to have raw EEG signal. Run command below:

# sudo apt-get install libmcrypt-dev libusb-1.0-0-dev libhidapi-dev liboscpack-dev

2) Go to emokit_lib package to build and install libemokit for future use:
# cd emokit_lib/

Configure source code
# cmake .
	Note: do cmake with a dot ".", which means Makefile will be created at current
 		top directory
Build library and install:
# sudo make; sudo make install

3) Go to openvibe package to build openvibe library. All needed stuffs had been
added to openvibe package. You just need to do below to build and use.

Check Linux package dependencies:
# cd openvibe-1.1.0-src/scripts/
# sudo ./linux-install_dependencies

Build openvibe
# sudo ./linux_build

Wait for completion and start to play with openvibe toolbox in dist/ directory

# cd dist/

We will use 2 scripts below:
- openvibe-acquisition-server.sh: for signal acquisition
- openvibe-designer.sh: for running modules

# sudo ./openvibe-acquisition-server.sh
Choose emokit and start/play OSC server

# sudo ./openvibe-designer.sh
Click open and point to my_p300_code/ directory. Select .xml file to open
modules
- p300-xdawn-0-signal-monitoring.xml: test signal quality before using.
- 300-xdawn-1-acquisition.xml: record sample raw data to prepare for training. 
- p300-xdawn-2-train-xDAWN.xml: train to find xdawn spatial filter.
- p300-xdawn-3-train-classifier.xml: train to classify samples.
- p300-xdawn-4-online.xml: main module.
- p300-xdawn-5-replay.xml: replay the main module.

//////////////////////////////

NOTES:
For main module (p300-xdawn-4-online.xml), it contains the python code for
interfaces and transfering commands to Raspberry Pi 2 board via Ethernet
subsystem. You can delete this python box if you dont want this.



